#!/bin/bash
#systemd required; adjust lines 13/14 for non-systemd

# change for new proxy details
proxy=1.1.1.1:80

mkdir /etc/systemd/system/cbagentd.service.d
tee /etc/systemd/system/cbagentd.service.d/custom.conf << EOF
[Service]
EnvironmentFile=/etc/systemd/system/cbagentd.service.d/custom.conf
https_proxy=$proxy
EOF
systemctl daemon-reload
systemctl restart cbagentd