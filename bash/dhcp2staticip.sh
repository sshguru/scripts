#!/bin/bash

# get current IP settings
IP=$(hostname -I | awk '{print $1}')
GW=$(ip route | head -n 1 | awk '{print $3}')
NM=$(ip -4 addr | grep $IP | awk '{ print $2 }' | sed 's/^[^/:]*[/:]//')
DNS1=$(cat /etc/resolv.conf | grep nameserver | awk '{if(NR==1) print $2}')
DNS2=$(cat /etc/resolv.conf | grep nameserver | awk '{if(NR==2) print $2}')
DNS3=$(cat /etc/resolv.conf | grep nameserver | awk '{if(NR==3) print $2}')

# get network card details
DEV=$(ip addr | grep $IP | awk '{print $NF}')
MAC=$(ip link show $DEV | grep link/ether | awk '{print $2}')
UUID=$(uuidgen $DEV)

# use nmcli for systems with NetworkManager service; otherwise use network-scriptis
if systemctl is-active --quiet NetworkManager
then 
	nmcli con mod System\ $DEV ipv4.addresses "$IP/$NM"
	nmcli con mod System\ $DEV ipv4.gateway "$GW"
	nmcli con mod System\ $DEV ipv4.dns "$DNS1 $DNS2 $DNS3"
	nmcli con mod System\ $DEV ipv4.method manual
	nmcli con mod System\ $DEV connection.autoconnect yes
	nmcli connection reload
else
	tee /etc/sysconfig/network-scripts/ifcfg-$DEV << EOF
HWADDR=$MAC
TYPE=Ethernet
BOOTPROTO=none
IPADDR=$IP
PREFIX=$NM
GATEWAY=$GW
DNS1=$DNS1
DNS2=$DNS2
DNS3=$DNS3
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
NAME=$DEV
UUID=$UUID
DEVICE=$DEV
ONBOOT=yes
EOF
	systemctl restart network
fi
