# get vcenter hostname/fqdn from command line
$vcenter = $args[0]

# connect to the vCenter server
Import-Module -Name VMware.PowerCLI
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore
Connect-VIServer -Server $vcenter

# get all IPs reported by VMware Tools
$vms = Get-VM | Select-Object @{N="IP";E={@($_.guest.IPAddress[0])}} | Select-Object -expand IP

# nslookup each IP; IPs which can't be resolved go into $fails variable
foreach ($vm in $vms) {
    $fails = $fails + $(nslookup $vm 2>&1)
}

# go through IPs recorded in previous step one by one
foreach ($fail in $fails){
    # extract IP addresses from nslookup error string
    $fail | Select-String -Pattern "Non-existent" | foreach-object { 
        $line=$_ -split " "
        $ipaddress = $line[4] 
        # discard IPv6 addresses and boxes which couldn't reach DHCP (169.254.x.x)
        if (!($ipaddress -Match "fe") -And !($ipaddress -Match "169.254")){
            # this bit converts IPv4 into a network address
            $array = New-Object System.Collections.ArrayList
            $ipaddress.Split(".") | ForEach-Object {$array.Add($_)} | Out-Null
            $array.Remove($array[3])
            $networks = $networks + $($array -join ".") + ".0`n"
        }
    }
}

# display result and clean up
$networks > networks.txt
Get-Content networks.txt | Sort-Object | get-unique > result.txt
Get-Content result.txt
Remove-Item networks.txt
Remove-Item result.txt